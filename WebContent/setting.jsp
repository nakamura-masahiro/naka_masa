<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
                <a href="management">ユーザー管理</a>
            </div>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <form action="setting" method="post"><br />
                <label for="account">アカウント</label>
                <input type="text" name="account" id="account" value="${editUser.account}" /> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

				<label for="password">確認用パスワード</label>
                <input name="password" type="password" id="password" /> <br />

				<label for="name">名前</label>
                <input type="text" name="name" id="name" value="${editUser.name}" /> <br />

                <label for="branchId">支社</label>
                <select name="branchId">
                	<c:forEach items="${branches}" var="branch">
                		<option value="${branch.id}" <c:if test="${branch.id==editUser.branchId}">selected</c:if>>${branch.name}</option>
                	</c:forEach>
                </select> <br />

                <label for="departmentId">部署</label>
                <select name="departmentId">
                	<c:forEach items="${departments}" var="department">
                		<option value="${department.id}" <c:if test="${department.id==editUser.departmentId}">selected</c:if>>${department.name}</option>
                	</c:forEach>
                </select> <br />

				<input type="hidden" name="editUserId" value="${editUser.id}">
                <input type="submit" value="更新" /> <br />
            </form>

            <div class="copyright">Copyright(c)Nakamura Masahiro</div>
        </div>
	</body>
</html>