<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
	</head>
	<body>
		<div class="main-contents">
            <div class="header">
                <a href="./">ホーム</a>
                <a href="signup">ユーザー新規登録</a>
            </div>

            <div class="user-informations">
            	<c:forEach items="${userInformations}" var="userInformation">
            		<div class="user-information">
            			<div class="account"><c:out value="${userInformation.account}" /></div>
            			<div class="name"><c:out value="${userInformation.name}" /></div>
						<div class="branch-name"><c:out value="${userInformation.branchName}" /></div>
			            <div class="department-name"><c:out value="${userInformation.departmentName}" /></div>
			            <div class="is-stopped">
			            	<c:if test="${userInformation.isStopped==0}">
								<c:out value="稼働中" />
							</c:if>
			            	<c:if test="${userInformation.isStopped==1}">
								<c:out value="停止中" />
							</c:if>
			            </div>
            		</div>
            		<div class="setting">
						<form action="setting" method="get">
							<input type="hidden" name="userInformationId" value="${userInformation.id}">
							<input type="submit" value="編集">
						</form>
						<form action="stop" method="post">
							<input type="hidden" name="userInformationId" value="${userInformation.id}">
							<input type="hidden" name="userInformationIsStopped" value="${userInformation.isStopped}">
							<c:if test="${userInformation.isStopped==0}">
								<input type="submit" value="停止">
							</c:if>
							<c:if test="${userInformation.isStopped==1}">
								<input type="submit" value="復活">
							</c:if>
						</form>
					</div>
            	</c:forEach>
            </div>

            <div class="copyright"> Copyright(c)Nakamura Masahiro</div>
		</div>
	</body>
</html>