package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<UserMessage> select(String start, String end, String category) {
        final int LIMIT_NUM = 1000;

        String startDatetime = "";
        String endDatetime = "";

        if((start == null) || (start.isEmpty())) {
        	startDatetime = "2020-01-01 00:00:00";
        } else {
        	startDatetime = start + " 00:00:00";
        }

        if((end == null) || (end.isEmpty())) {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date = new Date();
        	endDatetime = sdf.format(date);
        } else {
        	endDatetime = end + " 23:59:59";
        }

        Connection connection = null;
        try {
            connection = getConnection();
            List<UserMessage> messages = new UserMessageDao().select(connection,
            		LIMIT_NUM, startDatetime, endDatetime, category);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
