package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String loginServletPath = "/login";
		String servletPath = ((HttpServletRequest) request).getServletPath();
		//System.out.println(servletPath);

        HttpSession session = ((HttpServletRequest) request).getSession();
        User user = (User) session.getAttribute("loginUser");

		if(servletPath.equals(loginServletPath)) {
			chain.doFilter(request, response);// サーブレットを実行
		} else {
			if(user == null) {
	        	// userがnullならば、ログイン画面へ
	        	((HttpServletResponse) response).sendRedirect("./login");
	        } else {
	        	// userがnullでなければ、通常どおりの遷移
	            chain.doFilter(request, response);// サーブレットを実行
	        }
		}
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}
}
