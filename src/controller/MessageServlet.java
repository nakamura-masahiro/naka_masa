package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Message message = getMessage(request);
        if (!isValid(message, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./message");
            return;
        }

        User user = (User) session.getAttribute("loginUser");
        message.setUserId(user.getId());

        new MessageService().insert(message);
        response.sendRedirect("./");
    }

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException{

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));
		return message;
	}

    private boolean isValid(Message message, List<String> errorMessages) {

    	String title = message.getTitle();
    	String category = message.getCategory();
    	String text = message.getText();

    	if (StringUtils.isEmpty(title)) {
            errorMessages.add("件名を入力してください");
        } else if (30 < title.length()) {
            errorMessages.add("30文字以下で入力してください");
        }

    	if (StringUtils.isEmpty(category)) {
            errorMessages.add("カテゴリを入力してください");
        } else if (10 < category.length()) {
            errorMessages.add("10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("投稿内容を入力してください");
        } else if (1000 < text.length()) {
            errorMessages.add("1000文字以下で入力してください");
        }

        if (errorMessages.size() == 0) {
            return true;
        }
        return false;
    }
}
