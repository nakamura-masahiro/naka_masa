package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//支社取得
		List<Branch> branches = new BranchService().select();
		//支社をリクエストにセット
        request.setAttribute("branches", branches);

        //部署取得
		List<Department> departments = new DepartmentService().select();
		//部署をリクエストにセット
	    request.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException{

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
		new UserService().insert(user);
        response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

    	String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }

        if (!StringUtils.isEmpty(name) && (10 < name.length())) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        if (errorMessages.size() == 0) {
            return true;
        }
        return false;
    }
}
