package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		//メッセージ取得
		List<UserMessage> messages = new MessageService().select(start, end, category);
		//メッセージをリクエストにセット
        request.setAttribute("messages", messages);

        //コメント取得
  		List<UserComment> comments = new CommentService().select();
  		//コメントをリクエストにセット
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
